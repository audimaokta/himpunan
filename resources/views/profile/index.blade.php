@extends('layouts.master')

@section('title')
    Edit Profile
@endsection

@section('konten')
<div>
    <form action="/profile/{{$profile->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="angkatan">angkatan</label>
            <input type="text" class="form-control" name="angkatan" value="{{$profile->user->angkatan}}" disabled>
        </div>
        <div class="form-group">
            <label for="NIM">NIM</label>
            <input type="text" class="form-control" name="NIM" value="{{$profile->NIM}}" id="NIM" placeholder="Masukkan NIM">
            @error('NIM')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="jurusan">Jurusan</label>
            <input type="text" class="form-control" name="jurusan" value="{{$profile->jurusan}}" id="jurusan" placeholder="Masukkan jurusan">
            @error('jurusan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection