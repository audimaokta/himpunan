@extends('layouts.master')

@section('title')
<h2>Edit Informasi {{$mahasiswa->id}}</h2>
@endsection

@section('konten')
<div>
    <form action="/mahasiswa/{{$mahasiswa->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$mahasiswa->nama}}" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Angkatan</label>
            <input type="text" class="form-control" name="angkatan"  value="{{$mahasiswa->angkatan}}"  id="angkatan" placeholder="Masukkan umur">
            @error('angkatan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Departemen</label>
            <input type="text" class="form-control" name="departemen" value="{{$mahasiswa->departemen}}" id="departemen" placeholder="Masukkan bio">
            @error('departemen')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Jabatan</label>
            <input type="text" class="form-control" name="jabatan" value="{{$mahasiswa->jabatan}}" id="jabatan" placeholder="Masukkan bio">
            @error('jabatan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-success">Edit</button>
        <button type="submit" class="btn btn-light btn-sm my-2">Kembali</a>
    </form>
</div>
@endsection