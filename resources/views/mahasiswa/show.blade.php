@extends('layouts.master')

@section('konten')

    <h2>{{$mahasiswa->nama}}</h2>
    <p>Angkatan : {{$mahasiswa->angkatan}}</p>
    <p>Departement : {{$mahasiswa->departemen}}</p>
    <p>Jabatan : {{$mahasiswa->jabatan}}</p>

    <a href="/mahasiswa " class="btn btn-light btn-sm my-2">Kembali</a>
@endsection

