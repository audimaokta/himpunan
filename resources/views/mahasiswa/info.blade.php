@extends('layouts.master')

@section('konten')
<a href="/mahasiswa " class="btn btn-light btn-sm my-2">Kembali</a>

<h1>Info Departemen / Biro</h1>

<div class="card text-center">
    <div class="card-header">
     <H2>INTERNAL</H2>
    </div>
    <div class="card-body">
      <h2 class="card-title"></h2>
      <p class="card-text">Terdiri dari Ketua Himpunan, Wakil Ketua Himpunan, Supervisor, Sekretaris dan Bendahara</p>
      <p>Memiliki tugas untuk membimbing, mengawasi, serta melakukan evaluasi terhadap kegiatan yang dilakukan oleh tiap departemen atau biro, dan memberikan saran atau 
          rekomendasi mengenai kebijakan bersama.</p>
    </div>
    <div class="card-footer text-muted">
    </div>
  </div>

  <div class="card text-center">
    <div class="card-header">
     <H2>PSDM</H2>
    </div>
    <div class="card-body">
      <h2 class="card-title"></h2>
      <p class="card-text">Departemen yang bertanggung jawab dalam melaksanakan pembentukan karakter serta 
          pengembangan potensi sumber daya mahasiswa. Selain itu, bertanggung jawab untuk mempersiapkan sumber daya mahasiswa
        pemimpin untuk menjadi pemimpin di masa mendatang</p>
    </div>
    <div class="card-footer text-muted">
    </div>
  </div>

  <div class="card text-center">
    <div class="card-header">
     <H2>SOSIAL MASYARAKAT</H2>
    </div>
    <div class="card-body">
      <h2 class="card-title"></h2>
      <p class="card-text">Departemen yang bertanggung jawab melakukan peningkatan kualitas dan kuantitas dalam bidang aksi sosial dan kepedulian
          lingkngan. Juga bertanggung jawab dalam melaksanakan pengabdian dalam sosial masyarakatan guna meningkatkan kepekaan sosial mahasiswa serta 
        mendorong kegiatan pelestarian lingkungan yang memberikan kesejahteraan bagi masyarakat.</p>
    </div>
    <div class="card-footer text-muted">
    </div>
  </div>

  <div class="card text-center">
    <div class="card-header">
     <H2>KEWIRAUSAHAAN</H2>
    </div>
    <div class="card-body">
      <h2 class="card-title"></h2>
      <p class="card-text">Biro yang bertanggung jawab dalam menunjang EMTI dari segi finansial secara mandiri melalui usaha bisnis keratif maupun kemitraan.</p>
    </div>
    <div class="card-footer text-muted">
    </div>
  </div>

  <div class="card text-center">
    <div class="card-header">
     <H2>ADVOKASI</H2>
    </div>
    <div class="card-body">
      <h2 class="card-title"></h2>
      <p class="card-text">Departemen yang bertanggung jawab menghimpun permasalahan administrasi mahasiswa baik permasalahan akademik atau keuangan.</p>
    </div>
    <div class="card-footer text-muted">
    </div>
  </div>
  
  
@endsection